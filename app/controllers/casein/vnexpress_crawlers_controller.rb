module Casein
  class VnexpressCrawlersController < Casein::CaseinController
    def index
      @casein_page_title = 'Crawlers'
      @links = VnexpressCrawler.order(sort_order(:id)).paginate page: params[:page]
    end

    def upload
      if request.post?
        file = params[:file]
        file_content = file.read
        if file_content
          file_content = '[' + file_content + ']'
          list_obj = JSON.parse(file_content)
          success = true
          begin
            list_obj.each do |obj|
              VnexpressCrawler.transaction do
                VnexpressCrawler.find_or_create_by!(url: obj['url'], brand: obj['brand'])
              end
            end
          rescue Exception => e
            success = false
            redirect_to casein_vnexpress_crawlers_path, alert: "exception: #{e}"
          ensure
            file.close
          end
          redirect_to casein_vnexpress_crawlers_path, notice: 'Successfully import links'
        else
          redirect_to casein_vnexpress_crawlers_path, alert: 'Something wrong happened! Please try again!'
        end
      end
    end
  end
end
