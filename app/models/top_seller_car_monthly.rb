# == Schema Information
#
# Table name: top_seller_car_monthlies
#
#  id          :integer          not null, primary key
#  car_1_id    :integer
#  car_1_name  :string(255)
#  car_2_id    :integer
#  car_2_name  :string(255)
#  car_3_id    :integer
#  car_3_name  :string(255)
#  car_4_id    :integer
#  car_4_name  :string(255)
#  car_5_id    :integer
#  car_5_name  :string(255)
#  car_6_id    :integer
#  car_6_name  :string(255)
#  car_7_id    :integer
#  car_7_name  :string(255)
#  car_8_id    :integer
#  car_8_name  :string(255)
#  car_9_id    :integer
#  car_9_name  :string(255)
#  car_10_id   :integer
#  car_10_name :string(255)
#  date        :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class TopSellerCarMonthly < ApplicationRecord
  belongs_to :car_1, foreign_key: :car_1_id, class_name: 'Car'
  belongs_to :car_2, foreign_key: :car_2_id, class_name: 'Car'
  belongs_to :car_3, foreign_key: :car_3_id, class_name: 'Car'
  belongs_to :car_4, foreign_key: :car_4_id, class_name: 'Car'
  belongs_to :car_5, foreign_key: :car_5_id, class_name: 'Car'
  belongs_to :car_6, foreign_key: :car_6_id, class_name: 'Car'
  belongs_to :car_7, foreign_key: :car_7_id, class_name: 'Car'
  belongs_to :car_8, foreign_key: :car_8_id, class_name: 'Car'
  belongs_to :car_9, foreign_key: :car_9_id, class_name: 'Car'
  belongs_to :car_10, foreign_key: :car_10_id, class_name: 'Car'

  scope :current_month_list, -> { where(date: Date.current.strftime('%m%Y')) }
  scope :of_time, -> (time) { where(date: time) }
end
