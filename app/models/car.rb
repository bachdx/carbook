# == Schema Information
#
# Table name: cars
#
#  id                             :integer          not null, primary key
#  name                           :string(255)
#  car_group_id                   :integer
#  dai_rong_cao                   :string(255)
#  so_cho                         :integer
#  dung_tich_binh_xang            :integer
#  dong_co                        :string(255)
#  cong_suat                      :integer
#  momen_xoan                     :integer
#  khoang_cach_gam                :integer
#  duong_kinh_vong_quay_toi_thieu :integer
#  nguon_goc                      :integer
#  kind                           :integer
#  brand_id                       :integer
#  hop_so                         :string(255)
#  muc_tieu_thu_nhien_lieu        :string(255)
#  gia_niem_yet                   :string(255)
#  gia_dam_phan                   :string(255)
#  last_update_price_date         :date
#  image_url                      :string(255)
#  lock_version                   :integer
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#

class Car < ApplicationRecord
  has_many :car_gears
  has_many :monthly_reports
  belongs_to :brand
  belongs_to :car_group
  has_many :articles
  has_many :gears, through: :car_gears

  validates_presence_of :name, :dai_rong_cao
  validates_numericality_of :dung_tich_binh_xang, greater_than: 0
  validates :car_group_id, presence: true

  validate :validate_format_of_dai_rong_cao

  def validate_format_of_dai_rong_cao
    errors.add(:dai_rong_cao, 'wrong formated') unless dai_rong_cao =~ /\A\d{1,}x\d{1,}x\d{1,}\Z/
  end

  def fetch_basic_info(basic_info)
    assign_attributes(dai_rong_cao: basic_info[:dai_rong_cao]&.strip,
                      dung_tich_binh_xang: basic_info[:dung_tich_binh_xang],
                      dong_co: basic_info[:dong_co],
                      cong_suat: basic_info[:cong_suat],
                      momen_xoan: basic_info[:momen_xoan],
                      khoang_cach_gam: basic_info[:khoang_cach_gam],
                      duong_kinh_vong_quay_toi_thieu: basic_info[:duong_kinh_vong_quay_toi_thieu],
                      hop_so: basic_info[:hop_so],
                      kind: basic_info[:kind],
                      nguon_goc: basic_info[:nguon_goc],
                      muc_tieu_thu_nhien_lieu: basic_info[:muc_tieu_thu_nhien_lieu],
                      gia_niem_yet: basic_info[:gia_niem_yet],
                      gia_dam_phan: basic_info[:gia_dam_phan],
                      last_update_price_date: Date.current,
                      image_url: basic_info[:image_url],
                      brand_id: basic_info[:brand_id])

    self
  end

  class << self
    def api_get_all(options = {})
      page = options[:page]
      per_page = options[:per_page]

      result = order('id ASC')
      result = result.ransack(options[:s])
      result = result.result(distinct: true).paginate(page: page, per_page: per_page).includes(:brand)

      result
    end
  end
end
