# == Schema Information
#
# Table name: car_groups
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CarGroup < ApplicationRecord
  has_many :cars
  has_many :videos
end
