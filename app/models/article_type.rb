class ArticleType < ActiveHash::Base
  self.data = [
      { id: 1, code: "tn" , text: "Trong nước" },
      { id: 2, code: "qtxs", text: "Quốc tế" },
      { id: 3, code: "gtr", text: "Giải trí" },
      { id: 4, code: "hd", text: "Hỏi đáp" },
      { id: 5, code: "gkm", text: "Giá - KM" },
      { id: 6, code: "gt", text: " Giao thông" },
      { id: 7, code: "other", text: "Khác" },
      { id: 8, code: "xs", text: "Xe sang" },
      { id: 9, code: "xm", text: "Xe mới" }
  ]

  def json_builder
    {
        id: id,
        code: code,
        text: text
    }
  end
end