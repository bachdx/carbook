# == Schema Information
#
# Table name: articles
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  status            :integer
#  url               :string(255)
#  image_url         :string(255)
#  image_width       :integer
#  image_height      :integer
#  article_type_id   :integer
#  article_source_id :integer
#  color_code        :string(255)
#  source            :string(255)
#  lock_version      :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Article < ApplicationRecord
  attr_accessor :image

  validates :url, uniqueness: true

  enum status: { normal: 0, hot: 1 }

  class << self
    def api_get_all(options = {})
      page = options[:page]
      per_page = options[:per_page]

      result = order('created_at DESC')
      result = result.ransack(options[:s])
      result = result.result(distinct: true).paginate(page: page, per_page: per_page)

      result
    end
  end
end

