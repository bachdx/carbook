# == Schema Information
#
# Table name: videos
#
#  id           :integer          not null, primary key
#  title        :string(255)
#  car_group_id :integer
#  url          :string(255)
#  thumbnail    :string(255)
#  lock_version :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Video < ApplicationRecord
  attr_accessor :creator
  belongs_to :car_group

  after_validation :generate_thumbnail
  validates :url, uniqueness: true
  validates :car_group_id, presence: true
  # after_save :publish

  def generate_thumbnail
    youtube = Youtube.new(url)
    self.thumbnail = youtube.thumbnail
  end

  def publish
    $redis.publish 'video_created', {
      message: "#{car_group.name}'s new video is published recently",
      type: 'info',
      url: Rails.application.routes.url_helpers.casein_video_path(self),
      publisher_id: creator&.id || 9999999
    }.to_json
  end
end
