# == Schema Information
#
# Table name: monthly_reports
#
#  id           :integer          not null, primary key
#  car_id       :integer          not null
#  price        :integer
#  sold_number  :integer
#  time         :string(255)
#  lock_version :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class MonthlyReport < ApplicationRecord
  belongs_to :car
  delegate :car_group, :to => :car, :allow_nil => true
end
