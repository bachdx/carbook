class ArticleSource < ActiveHash::Base
  self.data = [
      { id: 1, code: "vnexpress" },
      { id: 2, code: "xehay" },
      { id: 3, code: "zing" },
      { id: 4, code: "autodaily" },
      { id: 5, code: "dantri.com.vn" },
      { id: 6, code: "other" },
  ]

  def json_builder
    {
        id: id,
        code: code
    }
  end
end