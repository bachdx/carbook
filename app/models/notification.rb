# == Schema Information
#
# Table name: notifications
#
#  id         :integer          not null, primary key
#  message    :string(255)
#  label      :string(255)
#  flag       :string(255)
#  flag_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'fcm'
class Notification < ApplicationRecord
  after_save :send_to_firebase

  ONE_TIME_SEND_NUMBER = 100.freeze

  def send_to_firebase
    fcm = FCM.new(FIREBASE_WEB_API_KEY)

    registration_ids = Registration.pluck(:device_id)
    return unless registration_ids
    options = {
      notification: {
        body: message,
        title: label
      },
      data: {
        flag: flag,
        id: flag_id
      }
    }
    begin
      registration_ids.each_slice(ONE_TIME_SEND_NUMBER) do |ar|
        fcm.send(ar, options)
      end
    rescue => e
      Rails.logger.info("Send message to FireBase failed : #{e.message}")
      return
    end
  end
end
