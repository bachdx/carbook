# == Schema Information
#
# Table name: gears
#
#  id           :integer          not null, primary key
#  name         :string(255)
#  lock_version :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Gear < ApplicationRecord
  has_many :car_gears
end
