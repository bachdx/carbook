# == Schema Information
#
# Table name: top_seller_car_monthlies
#
#  id          :integer          not null, primary key
#  car_1_id    :integer
#  car_1_name  :string(255)
#  car_2_id    :integer
#  car_2_name  :string(255)
#  car_3_id    :integer
#  car_3_name  :string(255)
#  car_4_id    :integer
#  car_4_name  :string(255)
#  car_5_id    :integer
#  car_5_name  :string(255)
#  car_6_id    :integer
#  car_6_name  :string(255)
#  car_7_id    :integer
#  car_7_name  :string(255)
#  car_8_id    :integer
#  car_8_name  :string(255)
#  car_9_id    :integer
#  car_9_name  :string(255)
#  car_10_id   :integer
#  car_10_name :string(255)
#  date        :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class TopSellerCarMonthlySerializer < ActiveModel::Serializer
  cars_list = [:car_1, :car_2, :car_3, :car_4, :car_5, :car_6, :car_7, :car_8, :car_9, :car_10]
  top_cars = []

  cars_list.each do |car|
    belongs_to car, serializer: TopSellerCarSerializer
    define_method car do
      car_object = object.send(car)
      car_object&.name = object.send("#{car}_name")

      car_object
    end
  end
end
