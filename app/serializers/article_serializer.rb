# == Schema Information
#
# Table name: articles
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  status            :integer
#  url               :string(255)
#  image_url         :string(255)
#  image_width       :integer
#  image_height      :integer
#  article_type_id   :integer
#  article_source_id :integer
#  color_code        :string(255)
#  source            :string(255)
#  lock_version      :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class ArticleSerializer < ActiveModel::Serializer
  attributes :id, :name, :color_code, :type, :source, :url, :article_image

  def type
    type = ArticleType.find(object.article_type_id)
    return {} unless type
    {
        id: type.id,
        code: type.code,
        text: type.text
    }
  end

  def article_image
    {
        url: object.image_url,
        width: object.image_width,
        height: object.image_height
    }
  end

  def source
    source = ArticleSource.find(object.article_source_id)
    return {} unless source
    {
        id: source.id,
        code: source.code
    }
  end
end
