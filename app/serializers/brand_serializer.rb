# == Schema Information
#
# Table name: brands
#
#  id           :integer          not null, primary key
#  name         :string(255)
#  desc         :text(65535)
#  lock_version :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class BrandSerializer < ActiveModel::Serializer
  attributes :id, :name, :desc
end
