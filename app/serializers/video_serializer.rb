# == Schema Information
#
# Table name: videos
#
#  id           :integer          not null, primary key
#  title        :string(255)
#  car_group_id :integer
#  url          :string(255)
#  thumbnail    :string(255)
#  lock_version :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class VideoSerializer < ActiveModel::Serializer
  attributes :id, :title, :car_group_id, :url, :thumbnail
end
