# == Schema Information
#
# Table name: car_gears
#
#  id           :integer          not null, primary key
#  car_id       :integer          not null
#  gear_id      :integer          not null
#  desc         :string(255)
#  lock_version :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class CarGearSerializer < ActiveModel::Serializer
  attributes :id, :gear, :desc

  belongs_to :gear, serializer: GearSerializer
end
