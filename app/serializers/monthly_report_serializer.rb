# == Schema Information
#
# Table name: monthly_reports
#
#  id           :integer          not null, primary key
#  car_id       :integer          not null
#  price        :integer
#  sold_number  :integer
#  time         :string(255)
#  lock_version :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class MonthlyReportSerializer < ActiveModel::Serializer
  attributes :car_id, :price, :sold_number, :time
end
