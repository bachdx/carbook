class TopSellerCarSerializer < ActiveModel::Serializer
  attributes :id, :image_url, :name, :car_group, :videos

  def videos
    object&.car_group&.videos&.order(updated_at: :desc)
  end

  def car_group
    object.car_group
  end
end
