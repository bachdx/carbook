# == Schema Information
#
# Table name: cars
#
#  id                             :integer          not null, primary key
#  name                           :string(255)
#  car_group_id                   :integer
#  dai_rong_cao                   :string(255)
#  so_cho                         :integer
#  dung_tich_binh_xang            :integer
#  dong_co                        :string(255)
#  cong_suat                      :integer
#  momen_xoan                     :integer
#  khoang_cach_gam                :integer
#  duong_kinh_vong_quay_toi_thieu :integer
#  nguon_goc                      :integer
#  kind                           :integer
#  brand_id                       :integer
#  hop_so                         :string(255)
#  muc_tieu_thu_nhien_lieu        :string(255)
#  gia_niem_yet                   :string(255)
#  gia_dam_phan                   :string(255)
#  last_update_price_date         :date
#  image_url                      :string(255)
#  lock_version                   :integer
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#

class CarSerializer < ActiveModel::Serializer
  attributes :id, :name, :image_url, :gia_niem_yet, :gia_dam_phan, :kind, :nguon_goc, :videos, :so_cho
  Car.attribute_names.each { |attr| attribute attr, if: :detail? }

  belongs_to :brand
  belongs_to :car_group
  has_many :car_gears, if: :detail?

  def videos
    if detail?
      instance_options[:videos] = object&.car_group&.videos&.order(updated_at: :desc)
    else
      videos = object&.car_group&.videos
      instance_options[:all_videos] ? videos.order(updated_at: :desc) : videos&.order(updated_at: :desc).last(5)
    end
  end

  def kind
    MASTERS[:CAR][:KIND][object.kind]
  end

  def gia_niem_yet
    object.gia_niem_yet&.delete(".")&.to_i
  end

  def gia_dam_phan
    object.gia_dam_phan&.delete(".")&.to_i
  end

  def nguon_goc
    MASTERS[:CAR][:NGUONGOC][object.nguon_goc]
  end

  def detail?
    instance_options[:detail]
  end
end
