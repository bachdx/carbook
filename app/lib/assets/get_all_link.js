var datas = [];
var result;

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

$( ".bg_tr.up_level.brand" ).each(function(  ) {

    var car = $(this).data('url');
    var brand = $(this).data('brand');
    datas.push(
        JSON.stringify({url: "http://vnexpress.net" + car, brand: brand.capitalize() })
    );

    result = JSON.stringify(datas);
});

function download(filename, text) {
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    pom.setAttribute('download', filename);

    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
    }
    else {
        pom.click();
    }
}


download('data.txt', datas);