# Update : 2017/11/07

require 'open-uri'
module Crawler
  module Vnexpress
    def self.execute
      Rails.logger.info("[] START #{__method__.to_s.upcase}")
      list = VnexpressCrawler.pluck(:url, :brand)
      list.each do |url, brand|
        execute_single_page(url, brand)
      end

      server_info = ServerInfo.first_or_create
      server_info.last_updated_time = Date.current
      server_info.save!

      Rails.logger.info("update lasted update time")
      Rails.logger.info("[] END #{__method__.to_s.upcase}")
    end

    def self.execute_single_page(url, brand)
      Rails.logger.info("[] START #{__method__.to_s.upcase}")
      begin
        doc = parse_car_url(url)
        save_info(doc, brand)
      rescue => e
        Rails.logger.debug("Error: #{e.message}")
        Rails.logger.info("[] Cant access #{url}")
      end
      Rails.logger.info("[] END #{__method__.to_s.upcase}")
    end

    private

    def self.parse_car_url(url)
      url = url.gsub('http', 'https')
      Nokogiri::HTML(open(url))
    end

    def self.get_basic_info(doc, brand)
      return unless doc
      Rails.logger.info("[] START #{__method__.to_s.upcase}")
      basic_info = doc.css('div.detail_info div.detail_content')[0].css('div.line')
      return unless basic_info

      main_info = []
      basic_info.each do |info|
        main_info.push info.css('div.right_line').text
      end

      price = get_car_price(doc)
      image_url = get_car_image(doc)

      brand = Brand.find_or_create_by!(name: brand)

      Rails.logger.info("[] END #{__method__.to_s.upcase}")
      {
        dai_rong_cao: main_info[0],
        dung_tich_binh_xang: main_info[1],
        dong_co: main_info[2],
        cong_suat: main_info[3],
        momen_xoan: main_info[4],
        khoang_cach_gam: main_info[5],
        duong_kinh_vong_quay_toi_thieu: main_info[6],
        nguon_goc: MASTERS[:CAR][:NGUONGOC].key(main_info[7]),
        kind: MASTERS[:CAR][:KIND].key(main_info[8]),
        hop_so: main_info[9],
        muc_tieu_thu_nhien_lieu: main_info[10],
        gia_niem_yet: price[:gia_niem_yet],
        gia_dam_phan: price[:gia_dam_phan],
        image_url: image_url,
        brand_id: brand.id
      }
    end

    def self.get_car_image(doc)
      Rails.logger.info("[] START #{__method__.to_s.upcase}")
      image = doc.css('div.main_info img')

      image_url = if image
                    image.attr('src').value
                  else
                    Rails.logger.info("[] FAIL #{__method__.to_s.upcase}")
                    nil
      end

      Rails.logger.info("[] END #{__method__.to_s.upcase}")
      image_url
    end

    def self.get_car_name(doc)
      Rails.logger.info("[] START #{__method__.to_s.upcase}")
      name_div = doc.css('div.name')
      unless name_div
        Rails.logger.info("[] FAIL #{__method__.to_s.upcase}")
        return nil
      end
      Rails.logger.info("[] END #{__method__.to_s.upcase}")
      name_div.text
    end

    def self.get_car_gears(doc)
      return unless doc
      Rails.logger.info("[] START #{__method__.to_s.upcase}")
      gear_lines = doc.css('#list_info div.line.item')
      gears = {}
      gear_lines.each do |line|
        gear = line.css('div.left_line')
        desc = line.css('div.right_line')
        next unless gear && desc
        gears[gear.text.tr(':', '').strip] = desc.text.strip
      end
      Rails.logger.info("[] END #{__method__.to_s.upcase}")
      gears
    end

    def self.get_car_price(doc)
      return unless doc
      Rails.logger.info("[] START #{__method__.to_s.upcase}")
      box_price = doc.css('div.box_price div.wap')
      Rails.logger.info("[] END #{__method__.to_s.upcase}")
      {
        gia_niem_yet: box_price.css('div.right_line')[0].text,
        gia_dam_phan: box_price.css('div.right_line')[0].text
      }
    end

    def self.save_info(doc, brand)
      return unless doc
      Rails.logger.info("[] START #{__method__.to_s.upcase}")
      name = get_car_name(doc)

      ActiveRecord::Base.transaction do
        Rails.logger.info("[] START #{__method__} : Start get basic car info")
        # basic car infos
        basic_info = get_basic_info(doc, brand)
        car = Car.find_or_initialize_by(name: name)
        car.fetch_basic_info(basic_info)
        car.save!(validate: false)
        Rails.logger.info("[] END #{__method__} : End get basic car info")

        Rails.logger.info("[] START #{__method__} : Start get report car info")
        # report infos
        report = MonthlyReport.find_or_initialize_by(car_id: car.id, time: Time.current.strftime('%Y%m'))
        report.price = basic_info[:gia_niem_yet]
        report.save!
        Rails.logger.info("[] END #{__method__} : Start get report car info")
      end

      car = Car.find_by_name(name)
      Rails.logger.info("[] END #{__method__} : Start get gears info")
      # gears infos
      gears_list = get_car_gears(doc)
      gears_list.each do |gear_name, desc|
        ActiveRecord::Base.transaction do
          gear = Gear.find_or_create_by!(name: gear_name)
          car_gear = CarGear.find_or_create_by!(car_id: car.id, gear_id: gear.id)
          car_gear.desc = desc
          car_gear.save!
        end
      end
      Rails.logger.info("[] END #{__method__} : End get gears info")

      Rails.logger.info("[] END #{__method__.to_s.upcase}")
    end
  end
end
