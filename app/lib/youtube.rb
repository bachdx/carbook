class Youtube
  URL_FORMATS = {
    regular: /^(https?:\/\/)?(www\.)?youtube.com\/watch\?(.*\&)?v=(?<id>[^&]+)/,
    shortened: /^(https?:\/\/)?(www\.)?youtu.be\/(?<id>[^&]+)/,
    embed: /^(https?:\/\/)?(www\.)?youtube.com\/embed\/(?<id>[^&]+)/,
    embed_as3: /^(https?:\/\/)?(www\.)?youtube.com\/v\/(?<id>[^?]+)/,
    chromeless_as3: /^(https?:\/\/)?(www\.)?youtube.com\/apiplayer\?video_id=(?<id>[^&]+)/
  }.freeze
  INVALID_CHARS = /[^a-zA-Z0-9\:\/\?\=\&\$\-\_\.\+\!\*\'\(\)\,]/

  attr_accessor :url, :id, :thumbnail

  def initialize(url)
    self.url = url
    self.id = self.class.extract_video_id(self.url)
    self.thumbnail = "https://img.youtube.com/vi/#{id}/mqdefault.jpg"
  end

  class << self
    def has_invalid_chars?(youtube_url)
      !INVALID_CHARS.match(youtube_url).nil?
    end

    def extract_video_id(youtube_url)
      return nil if has_invalid_chars?(youtube_url)

      URL_FORMATS.values.each do |format_regex|
        match = format_regex.match(youtube_url)
        return match[:id] if match
      end
    end

    def embed_url(youtube_url, width = 420, height = 315)
      vid_id = extract_video_id(youtube_url)
      %(<iframe width="#{width}" height="#{height}" src="http://www.youtube.com/embed/#{vid_id}" frameborder="0" allowfullscreen></iframe>)
    end

    def regular_url(youtube_url)
      vid_id = extract_video_id(youtube_url)
      "http://www.youtube.com/watch?v=#{vid_id}"
    end

    def shortened_url(youtube_url)
      vid_id = extract_video_id(youtube_url)
      "http://youtu.be/#{vid_id}"
    end
  end
end
