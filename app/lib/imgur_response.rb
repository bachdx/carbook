# services/imgur_response.rb
class ImgurResponse
  attr_accessor :code, :access_token, :data, :uploaded_image_link, :width, :height
  def initialize(response)
    @code = response.code
    response = JSON.parse(response)
    if @code == 200
      @access_token = response['access_token'] if response['access_token']
      if response['data']
        @uploaded_image_link = response['data']['link'] if response['data']['link']
        @width = response['data']['width'] if response['data']['width']
        @height = response['data']['height'] if response['data']['height']
      end
    else
      raise 'Cant request Imgur'
    end
  end
end
