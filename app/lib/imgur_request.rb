# services/imgur_request.rb
require 'rest-client'

class ImgurRequest
  attr_accessor :base_url, :refresh_token, :client_id, :client_secret, :grant_type, :method

  IMAGE_UPLOAD_FOLDER = Rails.application.config.imgur_upload_folder
  ACCESS_TOKEN_URL = 'https://api.imgur.com/oauth2/token'.freeze
  IMAGE_UPLOAD_URL = 'https://api.imgur.com/3/image'.freeze
  IMAGE_DELETE_URL = 'https://api.imgur.com/3/image/'.freeze

  def initialize(options)
    @method = method
    raise ArgumentError, 'Wrong params' unless options[:refresh_token] && options[:client_id] &&
        options[:client_secret]

    @refresh_token = options[:refresh_token]
    @client_id = options[:client_id]
    @client_secret = options[:client_secret]
  end

  def get_access_token
    @grant_type = 'refresh_token'
    tries ||= 3
  rescue => e
    retry unless (tries -= 1).zero?
  else
    body = {
        refresh_token: @refresh_token,
        client_id: @client_id,
        client_secret: @client_secret,
        grant_type: @grant_type
    }
    res = RestClient.post(ACCESS_TOKEN_URL,
                          body)

    return ImgurResponse.new(res).access_token
  end

  def upload_image(_folder = '', access_token, image)
    upload_folder = IMAGE_UPLOAD_FOLDER
    body = {
        image: image.read,
        album: upload_folder
    }
    headers = {
        'Authorization' => "Bearer #{access_token}"
    }

    res = RestClient.post(IMAGE_UPLOAD_URL, body, headers)
    response = ImgurResponse.new(res)
    return response
  rescue => e
    raise e
  end
end
