module ApplicationHelper
  def kinds_collection
    MASTERS[:CAR][:KIND].invert
  end

  def cars_collection(allow_bank = false)
    cars = Car.all.map { |i| [i.name, i.id] }
    cars.unshift(['Please pick a car', nil]) if allow_bank
    cars
  end
end
