module V1
  class Articles < Grape::API
    include V1Base
    resources :articles do
      desc 'Get a single article by id'
      params do
        requires :id, type: Integer, desc: 'Article ID'
      end
      get ':id' do
        article = Article.find(params[:id])
        serialization = ActiveModelSerializers::SerializableResource.new(article)

        render_success({ article: serialization.as_json }, {})
      end

      desc 'Get all articles'
      params do
        optional :page, type: Integer, desc: 'page number'
        optional :per_page, type: Integer, desc: 'Number of articles per page'
        optional :s, type: Hash, desc: 'Search query' do
          optional :article_type_id_eq, type: Integer, desc: 'ID of type'
          optional :article_source_id_eq, type: Integer, desc: 'ID of type'
          optional :status_in, type: Array, desc: 'article status (0: normal, 1: hot)'
        end
      end
      get '/' do
        page = (params[:page] || 1).to_i
        per_page = (params[:per_page] || 10).to_i
        options = {
            page: page,
            per_page: per_page,
            s: params[:s]
        }
        articles = Article.api_get_all(options)

        serialization = ActiveModel::Serializer::CollectionSerializer.new(articles, each_serializer: ArticleSerializer)

        render_success({ articles: serialization.as_json }, pagination_dict(articles))
      end
    end
  end
end
