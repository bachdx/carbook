module V1
  class Registrations < Grape::API
    include V1Base
    resources :registrations do
      desc 'Store new device registration id'
      params do
        requires :device_id, type: String, desc: 'Device registration ID'
      end
      post '/' do
        device_registration_id = Registration.find_or_initialize_by(device_id: params[:device_id])
        if device_registration_id.save
          render_success('Updated device registration id', {})
        else
          render_error(500, 'Failed to save registration id')
        end
      end
    end
  end
end
