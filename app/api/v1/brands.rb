module V1
  class Brands < Grape::API
    include V1Base

    resources :brands do
      desc 'Get a single brand by id'
      params do
        requires :id, type: Integer, desc: 'Brand ID'
      end
      get ':id' do
        brand = Brand.find(params[:id])
        serialization = ActiveModelSerializers::SerializableResource.new(brand)

        render_success({ brand: serialization.as_json }, {})
      end

      desc 'Get all brands'
      params do
        optional :page, type: Integer, desc: 'page number'
        optional :per_page, type: Integer, desc: 'Number of brands per page'
      end
      get '/' do
        page = (params[:page] || 1).to_i
        per_page = (params[:per_page] || 10).to_i
        brands = Brand.order('id ASC').paginate(page: page, per_page: per_page)
        serialization = ActiveModel::Serializer::CollectionSerializer.new(brands, each_serializer: BrandSerializer)

        render_success({ brands: serialization.as_json }, pagination_dict(brands))
      end
    end
  end
end
