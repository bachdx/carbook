module V1
  class Cars < Grape::API
    include V1Base

    resources :cars do
      desc 'Get a single car by id'
      params do
        requires :id, type: Integer, desc: 'Car ID'
      end
      get ':id' do
        car = Car.includes(:brand, car_gears: [:gear]).find(params[:id])
        serialization = ActiveModelSerializers::SerializableResource.new(car, all_videos: true, detail: true)

        render_success({ car: serialization.as_json }, {})
      end

      desc 'Get all cars'
      params do
        optional :page, type: Integer, desc: 'page number'
        optional :per_page, type: Integer, desc: 'Number of cars per page'
        optional :s, type: Hash, desc: 'Search query' do
          optional :brand_id_eq, type: Integer, desc: 'ID of brand'
        end
      end
      get '/' do
        page = (params[:page] || 1).to_i
        per_page = (params[:per_page] || 10).to_i
        options = {
          page: page,
          per_page: per_page,
          s: params[:s]
        }
        cars = Car.api_get_all(options)

        serialization = ActiveModel::Serializer::CollectionSerializer.new(cars, each_serializer: CarSerializer)

        render_success({ cars: serialization.as_json }, pagination_dict(cars))
      end
    end
  end
end
