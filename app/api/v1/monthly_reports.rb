module V1
  class MonthlyReports < Grape::API
    include V1Base
    resources :monthly_reports do
      desc 'get report this month of a car'
      params do
        requires :car_id, type: Integer, desc: 'Car ID'
        optional :time, type: String, desc: 'Month'
      end
      get '/' do
        time = params[:time] || Time.current.strftime('%Y%m')
        render_success({ report: MonthlyReport.where(time: time, car_id: params[:car_id]) }, {})
      end
    end
  end
end