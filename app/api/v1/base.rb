require 'grape-swagger'

module V1
  class Base < Grape::API
    mount V1::Cars
    mount V1::Brands
    mount V1::Articles
    mount V1::TopSellerCarMonthlies
    mount V1::Common
    mount V1::MonthlyReports
    mount V1::Registrations

    add_swagger_documentation(
      api_version: 'v1',
      hide_documentation_path: true,
      mount_path: '/api/v1/swagger_doc',
      hide_format: true,
      schemes: ['https'],
      info: {
        title: 'Carbook APIs',
        description: 'Carbook APIs.',
        contact_name: 'bachdx',
        contact_email: 'bachdx.hut@gmail.com'
      }
    )
  end
end
