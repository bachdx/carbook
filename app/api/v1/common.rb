module V1
  class Common < Grape::API
    include V1Base

    desc 'Get all article types'
    resources :article_types do
      desc 'get all article types'
      get '' do
        render_success({ article_types: ArticleType.all.map(&:json_builder) }, {})
      end
    end

    resources :article_sources do
      desc 'get all article sources'
      get '' do
        render_success({ article_sources: ArticleSource.all.map(&:json_builder) }, {})
      end
    end

    resources :server_infos do
      desc 'get server info'
      get '' do
        server_info = ServerInfo.first
        render_success({ infos: nil }, {}) unless server_info
        infos = {
            last_updated_time: server_info.last_updated_time.strftime("%Y%m%d")
        }
        render_success({ infos: infos }, {})
      end
    end
  end
end
