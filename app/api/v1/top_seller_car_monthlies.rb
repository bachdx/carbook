module V1
  class TopSellerCarMonthlies < Grape::API
    include V1Base

    resources :top_seller_car_monthlies do
      params do
        optional :time, type: String, desc: 'format: mmYYYY / 012018'
      end
      desc 'Get Top 10 seller car of this month'
      get '/' do
        top_seller = params[:time] ? TopSellerCarMonthly.of_time(params[:time]).first : TopSellerCarMonthly.current_month_list.first

        serialization = ActiveModelSerializers::SerializableResource.new(top_seller)
        render_success(serialization.as_json, {})
      end
    end
  end
end
