# app/services/imgur_service.rb
class ImgurService
  attr_accessor :refresh_token, :client_id, :client_secret

  REFRESH_TOKEN = '0e160b3393eb252202db7e00f99234fdc53650fb'.freeze
  CLIENT_ID = '3d069f8482d1ac2'.freeze
  CLIENT_SECRET = '09a8af17d242e4eb3f2a4a1f0ecadd034d4723f4'.freeze

  def initialize(params = {})
    @refresh_token = params[:refresh_token] || REFRESH_TOKEN
    @client_id = params[:client_id] || CLIENT_ID
    @client_secret = params[:client_secret] || CLIENT_SECRET
  end

  def get_access_token
    options = {
      refresh_token: @refresh_token,
      client_id: @client_id,
      client_secret: @client_secret
    }
    request = ImgurRequest.new(options)
    request.get_access_token
  end

  def upload_image(folder = '', image)
    options = {
      refresh_token: @refresh_token,
      client_id: @client_id,
      client_secret: @client_secret
    }
    request = ImgurRequest.new(options)
    access_token = request.get_access_token
    request.upload_image(folder, access_token, image)
  end
end