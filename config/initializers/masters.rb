MASTERS = {
  CAR: {
    KIND: {
      1 => 'Sedan', 2 => 'Hatchback', 3 => 'MPV', 4 => 'SUV', 5 => 'SUV-Coupe',
      6 => 'SUV-Wagon', 7 => 'Pick-up', 8 => 'Coupe', 9 => 'Coupe 2 cửa', 10 => 'Coupe 4 cửa',
      11 => 'Crossover', 12 => 'Convertible', 13 => 'Roadster', 14 => 'Siêu xe', 15 => 'Station Wagon'
    },
    NGUONGOC: { 1 => 'Lắp ráp', 2 => 'Nhập khẩu' }
  }
}.freeze

API_VENDOR = 'carbook-api'.freeze

RESPONSE_CODE = {
  success: 200,
  bad_request: 400,
  unauthorized: 401,
  forbidden: 403,
  not_found: 404,
  unprocessable_entity: 422,
  internal_server_error: 500,
  common_error: 1001
}.freeze

FIREBASE_WEB_API_KEY = "AAAANsWHVQQ:APA91bGS8O92peY7OvMkHMKxKMv2kS87V6U6fGUuN56pdDHQt6N89LU_JYll8-OmXlVlJ_A8fqRaWRGEIlsq4Qny4ZOLjHq2fRba5cbyyLxhRWaCy7AvVk7QBddKBjzPSYprDLJo37Ps"