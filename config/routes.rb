# == Route Map
#
#                            Prefix Verb   URI Pattern                                       Controller#Action
#                   application_api        /                                                 ApplicationApi
#                     casein_videos GET    /casein/videos(.:format)                          casein/videos#index
#                                   POST   /casein/videos(.:format)                          casein/videos#create
#                  new_casein_video GET    /casein/videos/new(.:format)                      casein/videos#new
#                 edit_casein_video GET    /casein/videos/:id/edit(.:format)                 casein/videos#edit
#                      casein_video GET    /casein/videos/:id(.:format)                      casein/videos#show
#                                   PATCH  /casein/videos/:id(.:format)                      casein/videos#update
#                                   PUT    /casein/videos/:id(.:format)                      casein/videos#update
#                                   DELETE /casein/videos/:id(.:format)                      casein/videos#destroy
#            casein_monthly_reports GET    /casein/monthly_reports(.:format)                 casein/monthly_reports#index
#                                   POST   /casein/monthly_reports(.:format)                 casein/monthly_reports#create
#         new_casein_monthly_report GET    /casein/monthly_reports/new(.:format)             casein/monthly_reports#new
#        edit_casein_monthly_report GET    /casein/monthly_reports/:id/edit(.:format)        casein/monthly_reports#edit
#             casein_monthly_report GET    /casein/monthly_reports/:id(.:format)             casein/monthly_reports#show
#                                   PATCH  /casein/monthly_reports/:id(.:format)             casein/monthly_reports#update
#                                   PUT    /casein/monthly_reports/:id(.:format)             casein/monthly_reports#update
#                                   DELETE /casein/monthly_reports/:id(.:format)             casein/monthly_reports#destroy
#                      casein_gears GET    /casein/gears(.:format)                           casein/gears#index
#                                   POST   /casein/gears(.:format)                           casein/gears#create
#                   new_casein_gear GET    /casein/gears/new(.:format)                       casein/gears#new
#                  edit_casein_gear GET    /casein/gears/:id/edit(.:format)                  casein/gears#edit
#                       casein_gear GET    /casein/gears/:id(.:format)                       casein/gears#show
#                                   PATCH  /casein/gears/:id(.:format)                       casein/gears#update
#                                   PUT    /casein/gears/:id(.:format)                       casein/gears#update
#                                   DELETE /casein/gears/:id(.:format)                       casein/gears#destroy
#                  casein_car_gears GET    /casein/car_gears(.:format)                       casein/car_gears#index
#                                   POST   /casein/car_gears(.:format)                       casein/car_gears#create
#               new_casein_car_gear GET    /casein/car_gears/new(.:format)                   casein/car_gears#new
#              edit_casein_car_gear GET    /casein/car_gears/:id/edit(.:format)              casein/car_gears#edit
#                   casein_car_gear GET    /casein/car_gears/:id(.:format)                   casein/car_gears#show
#                                   PATCH  /casein/car_gears/:id(.:format)                   casein/car_gears#update
#                                   PUT    /casein/car_gears/:id(.:format)                   casein/car_gears#update
#                                   DELETE /casein/car_gears/:id(.:format)                   casein/car_gears#destroy
#                       casein_cars GET    /casein/cars(.:format)                            casein/cars#index
#                                   POST   /casein/cars(.:format)                            casein/cars#create
#                    new_casein_car GET    /casein/cars/new(.:format)                        casein/cars#new
#                   edit_casein_car GET    /casein/cars/:id/edit(.:format)                   casein/cars#edit
#                        casein_car GET    /casein/cars/:id(.:format)                        casein/cars#show
#                                   PATCH  /casein/cars/:id(.:format)                        casein/cars#update
#                                   PUT    /casein/cars/:id(.:format)                        casein/cars#update
#                                   DELETE /casein/cars/:id(.:format)                        casein/cars#destroy
#                     casein_brands GET    /casein/brands(.:format)                          casein/brands#index
#                                   POST   /casein/brands(.:format)                          casein/brands#create
#                  new_casein_brand GET    /casein/brands/new(.:format)                      casein/brands#new
#                 edit_casein_brand GET    /casein/brands/:id/edit(.:format)                 casein/brands#edit
#                      casein_brand GET    /casein/brands/:id(.:format)                      casein/brands#show
#                                   PATCH  /casein/brands/:id(.:format)                      casein/brands#update
#                                   PUT    /casein/brands/:id(.:format)                      casein/brands#update
#                                   DELETE /casein/brands/:id(.:format)                      casein/brands#destroy
#                   casein_articles GET    /casein/articles(.:format)                        casein/articles#index
#                                   POST   /casein/articles(.:format)                        casein/articles#create
#                new_casein_article GET    /casein/articles/new(.:format)                    casein/articles#new
#               edit_casein_article GET    /casein/articles/:id/edit(.:format)               casein/articles#edit
#                    casein_article GET    /casein/articles/:id(.:format)                    casein/articles#show
#                                   PATCH  /casein/articles/:id(.:format)                    casein/articles#update
#                                   PUT    /casein/articles/:id(.:format)                    casein/articles#update
#                                   DELETE /casein/articles/:id(.:format)                    casein/articles#destroy
#  upload_casein_vnexpress_crawlers GET    /casein/vnexpress_crawlers/upload(.:format)       casein/vnexpress_crawlers#upload
#                                   POST   /casein/vnexpress_crawlers/upload(.:format)       casein/vnexpress_crawlers#upload
#         casein_vnexpress_crawlers GET    /casein/vnexpress_crawlers(.:format)              casein/vnexpress_crawlers#index
#                             admin GET    /admin(.:format)                                  redirect(301, /casein)
# update_password_casein_admin_user PATCH  /casein/admin_users/:id/update_password(.:format) casein/admin_users#update_password
#  reset_password_casein_admin_user PATCH  /casein/admin_users/:id/reset_password(.:format)  casein/admin_users#reset_password
#                casein_admin_users GET    /casein/admin_users(.:format)                     casein/admin_users#index
#                                   POST   /casein/admin_users(.:format)                     casein/admin_users#create
#             new_casein_admin_user GET    /casein/admin_users/new(.:format)                 casein/admin_users#new
#            edit_casein_admin_user GET    /casein/admin_users/:id/edit(.:format)            casein/admin_users#edit
#                 casein_admin_user GET    /casein/admin_users/:id(.:format)                 casein/admin_users#show
#                                   PATCH  /casein/admin_users/:id(.:format)                 casein/admin_users#update
#                                   PUT    /casein/admin_users/:id(.:format)                 casein/admin_users#update
#                                   DELETE /casein/admin_users/:id(.:format)                 casein/admin_users#destroy
#     new_casein_admin_user_session GET    /casein/admin_user_session/new(.:format)          casein/admin_user_sessions#new
#         casein_admin_user_session DELETE /casein/admin_user_session(.:format)              casein/admin_user_sessions#destroy
#                                   POST   /casein/admin_user_session(.:format)              casein/admin_user_sessions#create
#        edit_casein_password_reset GET    /casein/password_reset/edit(.:format)             casein/password_resets#edit
#             casein_password_reset PATCH  /casein/password_reset(.:format)                  casein/password_resets#update
#                                   PUT    /casein/password_reset(.:format)                  casein/password_resets#update
#                                   POST   /casein/password_reset(.:format)                  casein/password_resets#create
#                      casein_blank GET    /casein/blank(.:format)                           casein/casein#blank
#                       casein_root GET    /casein(.:format)                                 casein/casein#index
#

Rails.application.routes.draw do
  # API
  mount ApplicationApi, at: '/'

  resources :infos, only: [:index]

  namespace :casein do
    resources :infos
    resources :notifications
    resources :car_groups
    resources :top_seller_car_monthlies
    resources :videos
    resources :monthly_reports
    resources :gears
    resources :car_gears
    resources :cars
    resources :brands
    resources :articles
    resources :vnexpress_crawlers, only: [:index] do
      collection do
        get :upload
        post :upload
      end
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
