source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.4'
# Use mysql as the database for Active Record
gem 'mysql2', '>= 0.3.18', '< 0.5'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'
gem 'casein', '~>5.3.0'
# Grape API
gem 'bootstrap-datepicker-rails'
gem 'grape'
gem 'grape-active_model_serializers'
gem 'grape-swagger'
gem 'grape_on_rails_routes'
gem 'nokogiri'
gem 'rack-cors', require: 'rack/cors'
gem 'ransack'
gem 'redis'
gem 'rest-client', '~> 2.0', '>= 2.0.2'
gem 'socket.io-rails'
gem 'unicorn'
gem 'active_hash'
gem 'jquery-minicolors-rails'
gem 'fcm'
gem 'redcarpet'

group :development, :test do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'listen', '~> 3.0.5'
  gem 'web-console'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'annotate', require: false
  gem 'awesome_print', require: 'ap' # re-format things in console
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'bullet'
  gem 'byebug'
  gem 'capistrano', require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano-rails', require: false
  gem 'capistrano-rvm', require: false
  gem 'capistrano3-puma', require: false
  gem 'capybara'
  gem 'factory_girl_rails', '~> 4.5'
  gem 'guard-rspec', require: false
  gem 'haml-lint'
  gem 'pry'
  gem 'pry-byebug'
  gem 'pry-doc'
  gem 'pry-rails'
  gem 'pry-stack_explorer'
  gem 'puffing-billy'
  gem 'rack-mini-profiler', require: false # show db executing time
  gem 'rails-erd' # draw and output a pdf file of erd
  gem 'rails-flog', require: 'flog' # re-format the hash and sql in log
  gem 'rspec-rails', '~> 3.2'
  gem 'rubocop'
  gem 'selenium-webdriver' # used by JavaScript-dependent feature specs (`js: true`)
  gem 'spring'
  gem 'spring-commands-rspec' # Enable Spring for RSpec
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'view_source_map' # insert the path name of a rendered partial view as HTML
end

group :test do
  gem 'database_cleaner'
  gem 'email_spec'
  gem 'faker'
  gem 'poltergeist' # helps with using PhantomJS headless browser in feature specs
  gem 'shoulda-matchers'
  gem 'vcr'
  gem 'webmock'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
