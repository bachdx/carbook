var express = require('express');
var app = express();
var server = require('http').createServer(app);
var socket = require('socket.io').listen(server);
var port = 8080;
var redis = require("redis");
server.listen(port);

var sub = redis.createClient();
sub.subscribe('video_created');
sub.on("message", function(channel, message) {
    console.log("from rails to subscriber:", channel, message);
    socket.emit(channel, message);
});

// Add a connect listener
socket.on('connection', function(client){
    console.log('Connection to client established');

    socket.on('disconnect', function () {
        console.log("user disconnected");
        sub.quit();
    });

});

console.log('Server running at http://127.0.0.1:' + port + '/');