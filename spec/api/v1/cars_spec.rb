require 'rails_helper'

describe V1::Cars do
  context 'GET a single car by id' do
    it 'return a car by id' do
      car = FactoryGirl.create(:normal_car)
      get "/api/cars/#{car.id}"

      expect(response.status).to eq 200
      body = JSON.parse(response.body)

      expect(body).to be_an_instance_of Hash
      expect(body).to include('data')
      expect(body['data']).to include('car')
      expect(body['data']['car']).to include('id' => car.id)
      expect(body['data']['car']).to include('name' => car.name)
      expect(body['data']['car']).to include('image_url' => car.image_url)
      expect(body['data']['car']).to include('videos')
      expect(body['data']['car']).to include('brand' => car.brand)

      videos = body['data']['car']['videos']
      expect(body['data']['car']).to include('videos' => car.videos.order(id: :desc).select(:id, :title, :car_id, :url, :thumbnail).as_json)
      expect(videos.size).to eq(10)
    end
  end

  context 'GET cars collection' do
    it 'should properly return cars' do
      cars_collection = FactoryGirl.create_list(:normal_car, 10)
      get '/api/cars/'
      expect(response.status).to eq 200
      body = JSON.parse(response.body)

      expect(body).to be_an_instance_of Hash
      expect(body).to include('data')
      expect(body['data']).to include('cars')
      cars = body['data']['cars']
      cars.each.with_index do |car, index|
        expect(car).to include('id' => cars_collection[index].id)
        expect(car).to include('name' => cars_collection[index].name)
        expect(car).to include('image_url' => cars_collection[index].image_url)
        expect(car).to include('brand' => cars_collection[index].brand)
      end
    end
  end
end
