# spec/services/imgur_request_spec.rb
require 'rails_helper'

describe ImgurRequest do
  context 'it should be initialized properly' do
    it 'should properly provided params' do
      params = {
        refresh_token: Faker::Crypto.md5,
        client_id: Faker::Crypto.md5,
        client_secret: Faker::Crypto.md5
      }

      imgur_request_service = ImgurRequest.new(params)
      expect(imgur_request_service).to have_attributes(
        refresh_token: params[:refresh_token],
        client_id: params[:client_id],
        client_secret: params[:client_secret]
      )
    end

    it 'should raise error when provided wrong params' do
      params = {
        refresh_token: Faker::Crypto.md5
      }
      expect { ImgurRequest.new(params) }.to raise_error(ArgumentError)
    end
  end

  context 'it should functional properly' do
    it 'should be able to get access token' do
      imgur_request = ImgurRequest.new(refresh_token: '0e160b3393eb252202db7e00f99234fdc53650fb',
                                       client_id: '3d069f8482d1ac2',
                                       client_secret: '09a8af17d242e4eb3f2a4a1f0ecadd034d4723f4')
      response = imgur_request.get_access_token
      expect(response).not_to be_nil
    end
  end

  context 'it should retry properly' do
  end
end
