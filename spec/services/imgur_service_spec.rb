# spec/services/imgur_service_spec
require 'rails_helper'

describe ImgurService do
  context 'initializer' do
    context 'when init imgur service' do
      it 'should properly provided params' do
        params = {
          refresh_token: Faker::Crypto.md5,
          client_id: Faker::Crypto.md5,
          client_secret: Faker::Crypto.md5
        }

        imgur_service = ImgurService.new(params)
        expect(imgur_service).to have_attributes(refresh_token: params[:refresh_token], client_id: params[:client_id], client_secret: params[:client_secret])
      end
    end
  end

  context 'Access Token and Refresh Token' do
    let(:imgur) { ImgurService.new }

    it 'should be abale to get new access token' do
      access_token = imgur.get_access_token
      expect(access_token).not_to be_nil
    end
  end
end
