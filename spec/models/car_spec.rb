# == Schema Information
#
# Table name: cars
#
#  id                             :integer          not null, primary key
#  name                           :string(255)
#  car_group_id                   :integer
#  dai_rong_cao                   :string(255)
#  so_cho                         :integer
#  dung_tich_binh_xang            :integer
#  dong_co                        :string(255)
#  cong_suat                      :integer
#  momen_xoan                     :integer
#  khoang_cach_gam                :integer
#  duong_kinh_vong_quay_toi_thieu :integer
#  nguon_goc                      :integer
#  kind                           :integer
#  brand_id                       :integer
#  hop_so                         :string(255)
#  muc_tieu_thu_nhien_lieu        :string(255)
#  gia_niem_yet                   :string(255)
#  gia_dam_phan                   :string(255)
#  last_update_price_date         :date
#  image_url                      :string(255)
#  lock_version                   :integer
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#

require 'rails_helper'

describe Car do
  context 'validations' do
    it { should validate_presence_of :name }
    it { should validate_presence_of :dai_rong_cao }
    it { should validate_numericality_of(:dung_tich_binh_xang).is_greater_than(0) }
  end

  it 'validates that dai_rong_cao is properly formated' do
    car = FactoryGirl.build(:normal_car, dai_rong_cao: 'aaa')
    car.valid?
    car1 = FactoryGirl.build(:normal_car, dai_rong_cao: '111x222x')
    car1.valid?
    car3 = FactoryGirl.build(:normal_car, dai_rong_cao: '111x222x333')
    car3.valid?
    expect(car.errors[:dai_rong_cao]).to include('wrong formated')
    expect(car1.errors[:dai_rong_cao]).to include('wrong formated')
    expect(car3.errors[:dai_rong_cao]).to eq([])
  end

  it '[fetch_basic_info spec] should fetch basic info properly' do
    car = FactoryGirl.build(:empty_car)
    basic_info = {
        dai_rong_cao: '100x200x300',
        dung_tich_binh_xang: 100,
        dong_co: 'aaa',
        cong_suat: 123,
        momen_xoan: 456,
        khoang_cach_gam: 789,
        duong_kinh_vong_quay_toi_thieu: 12,
        hop_so: '1223a',
        kind: 1,
        nguon_goc: 2,
        muc_tieu_thu_nhien_lieu: '1.5',
        gia_niem_yet: '797000000',
        gia_dam_phan: '797000000',
        last_update_price_date: Date.current,
        image_url: 'https://www.abc.def.png',
        brand_id: 1
    }
    car.fetch_basic_info(basic_info)

    %i[
      dai_rong_cao dung_tich_binh_xang dong_co cong_suat
      momen_xuan khoang_cach_gam duong_kinh_vong_quay_toi_thieu
      hop_so kind nguon_goc muc_tieu_thu_nhien_lieu
      gia_niem_yet gia_dam_phan last_update_price_date image_url
      brand_id
    ].each do |item|
      expect(car[item]).to eq basic_info[item]
    end
  end
end
