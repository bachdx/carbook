# == Schema Information
#
# Table name: videos
#
#  id           :integer          not null, primary key
#  title        :string(255)
#  car_group_id :integer
#  url          :string(255)
#  thumbnail    :string(255)
#  lock_version :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryGirl.define do
  factory :video do
    sequence(:title) { |n| "video #{n}" }
    url { Faker::Internet.url('youtube.com', "/watch?v=##{Faker::Internet.user_name}") }
  end
end
