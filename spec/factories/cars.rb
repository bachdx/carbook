# == Schema Information
#
# Table name: cars
#
#  id                             :integer          not null, primary key
#  name                           :string(255)
#  car_group_id                   :integer
#  dai_rong_cao                   :string(255)
#  so_cho                         :integer
#  dung_tich_binh_xang            :integer
#  dong_co                        :string(255)
#  cong_suat                      :integer
#  momen_xoan                     :integer
#  khoang_cach_gam                :integer
#  duong_kinh_vong_quay_toi_thieu :integer
#  nguon_goc                      :integer
#  kind                           :integer
#  brand_id                       :integer
#  hop_so                         :string(255)
#  muc_tieu_thu_nhien_lieu        :string(255)
#  gia_niem_yet                   :string(255)
#  gia_dam_phan                   :string(255)
#  last_update_price_date         :date
#  image_url                      :string(255)
#  lock_version                   :integer
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#

FactoryGirl.define do
  factory :normal_car, class: Car do
    sequence(:name) { |n| "car #{n}" }
    dai_rong_cao { "#{Faker::Number.number(10)}x#{Faker::Number.number(10)}x#{Faker::Number.number(10)}" }
    dung_tich_binh_xang { Faker::Number.number(5) }
    brand_id { Random.rand(10) }

    transient do
      videos_count 10
    end

    after(:create) do |car, evaluator|
      create_list(:video, evaluator.videos_count, car: car)
    end
  end

  factory :empty_car, class: Car do
  end
end
