# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180326071214) do

  create_table "articles", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.integer "status", default: 0
    t.string "url"
    t.string "image_url"
    t.integer "image_width"
    t.integer "image_height"
    t.integer "article_type_id"
    t.integer "article_source_id"
    t.string "color_code"
    t.string "source"
    t.integer "lock_version"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "brands", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.text "desc"
    t.integer "lock_version"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "car_gears", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "car_id", null: false
    t.integer "gear_id", null: false
    t.string "desc"
    t.integer "lock_version"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "car_groups", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cars", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.integer "car_group_id"
    t.string "dai_rong_cao"
    t.integer "so_cho"
    t.integer "dung_tich_binh_xang"
    t.string "dong_co"
    t.integer "cong_suat", comment: "ma luc"
    t.integer "momen_xoan", comment: "Nm"
    t.integer "khoang_cach_gam", comment: "mm"
    t.integer "duong_kinh_vong_quay_toi_thieu", comment: "m"
    t.integer "nguon_goc"
    t.integer "kind"
    t.integer "brand_id"
    t.string "hop_so"
    t.string "muc_tieu_thu_nhien_lieu"
    t.string "gia_niem_yet"
    t.string "gia_dam_phan"
    t.date "last_update_price_date"
    t.string "image_url"
    t.integer "lock_version"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "casein_admin_users", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "login", null: false
    t.string "name"
    t.string "email"
    t.integer "access_level", default: 0, null: false
    t.string "crypted_password", null: false
    t.string "password_salt", null: false
    t.string "persistence_token"
    t.string "single_access_token"
    t.string "perishable_token"
    t.integer "login_count", default: 0, null: false
    t.integer "failed_login_count", default: 0, null: false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string "current_login_ip"
    t.string "last_login_ip"
    t.string "time_zone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gears", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.integer "lock_version"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "infos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.text "content"
  end

  create_table "monthly_reports", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "car_id", null: false
    t.integer "price", comment: "trieu dong"
    t.integer "sold_number", comment: "chiec"
    t.string "time", comment: "yyyymm"
    t.integer "lock_version"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["time", "car_id"], name: "index_monthly_reports_on_time_and_car_id", unique: true
  end

  create_table "notifications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "message"
    t.string "label"
    t.string "flag"
    t.integer "flag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "registrations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "device_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "server_infos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.date "last_updated_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "top_seller_car_monthlies", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "car_1_id"
    t.string "car_1_name"
    t.integer "car_2_id"
    t.string "car_2_name"
    t.integer "car_3_id"
    t.string "car_3_name"
    t.integer "car_4_id"
    t.string "car_4_name"
    t.integer "car_5_id"
    t.string "car_5_name"
    t.integer "car_6_id"
    t.string "car_6_name"
    t.integer "car_7_id"
    t.string "car_7_name"
    t.integer "car_8_id"
    t.string "car_8_name"
    t.integer "car_9_id"
    t.string "car_9_name"
    t.integer "car_10_id"
    t.string "car_10_name"
    t.string "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "videos", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "title"
    t.integer "car_group_id"
    t.string "url"
    t.string "thumbnail"
    t.integer "lock_version"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "vnexpress_crawlers", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "url"
    t.string "brand"
    t.integer "lock_version"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
