class CreateServerInfos < ActiveRecord::Migration[5.1]
  def change
    create_table :server_infos do |t|
      t.date :last_updated_time

      t.timestamps
    end
  end
end
