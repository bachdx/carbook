class AddColumns < ActiveRecord::Migration[5.1]
  def change
    add_column :cars, :car_group_id, :integer, after: :name
    add_column :videos, :car_group_id, :integer, after: :title
    remove_column :videos, :car_id
  end
end
