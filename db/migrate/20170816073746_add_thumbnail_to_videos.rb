class AddThumbnailToVideos < ActiveRecord::Migration[5.0]
  def change
    add_column :videos, :thumbnail, :string, after: :url
  end
end
