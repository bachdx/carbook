class CreateCars < ActiveRecord::Migration[5.0]
  def change
    create_table :cars do |t|
      t.string :name
      t.string :dai_rong_cao
      t.integer :dung_tich_binh_xang
      t.string :dong_co
      t.integer :cong_suat, comment: 'ma luc'
      t.integer :momen_xoan, comment: 'Nm'
      t.integer :khoang_cach_gam, comment: 'mm'
      t.integer :duong_kinh_vong_quay_toi_thieu, comment: 'm'
      t.integer :nguon_goc
      t.integer :kind
      t.integer :brand_id
      t.string :hop_so
      t.string :muc_tieu_thu_nhien_lieu
      t.string :gia_niem_yet
      t.string :gia_dam_phan
      t.date :last_update_price_date
      t.string :image_url

      t.integer :lock_version
      t.timestamps
    end
  end
end
