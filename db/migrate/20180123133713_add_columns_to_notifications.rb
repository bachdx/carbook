class AddColumnsToNotifications < ActiveRecord::Migration[5.1]
  def change
    add_column :notifications, :flag, :string, after: :label
    add_column :notifications, :flag_id, :integer, after: :flag
  end
end
