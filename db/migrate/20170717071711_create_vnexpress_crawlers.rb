class CreateVnexpressCrawlers < ActiveRecord::Migration[5.0]
  def change
    create_table :vnexpress_crawlers do |t|
      t.string :url
      t.string :brand

      t.integer :lock_version
      t.timestamps
    end
  end
end
