class CreateCarGears < ActiveRecord::Migration[5.0]
  def change
    create_table :car_gears do |t|
      t.integer :car_id, null: false
      t.integer :gear_id, null: false
      t.string :desc

      t.integer :lock_version
      t.timestamps
    end
  end
end
