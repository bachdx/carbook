class DeleteKindInArticles < ActiveRecord::Migration[5.1]
  def change
    remove_column :articles, :kind
  end
end
