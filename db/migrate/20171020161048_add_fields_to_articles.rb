class AddFieldsToArticles < ActiveRecord::Migration[5.1]
  def change
    add_column :articles, :article_type_id, :integer, after: :image_url
    add_column :articles, :article_source_id, :integer, after: :article_type_id
    add_column :articles, :color_code, :string, after: :article_source_id
    add_column :articles, :source, :string, after: :color_code
  end
end