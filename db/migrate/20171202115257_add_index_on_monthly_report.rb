class AddIndexOnMonthlyReport < ActiveRecord::Migration[5.1]
  def change
    add_index :monthly_reports, [:time, :car_id], unique: true
  end
end
