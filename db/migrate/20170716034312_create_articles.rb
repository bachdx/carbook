class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.integer :car_id
      t.integer :kind
      t.string :name
      t.string :url

      t.integer :lock_version
      t.timestamps
    end
  end
end
