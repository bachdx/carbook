class AddHotArticles < ActiveRecord::Migration[5.1]
  def change
    add_column :articles, :status, :boolean, after: :name
  end
end
