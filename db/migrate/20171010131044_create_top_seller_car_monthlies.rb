class CreateTopSellerCarMonthlies < ActiveRecord::Migration[5.1]
  def change
    create_table :top_seller_car_monthlies do |t|
      t.integer :car_1_id
      t.string :car_1_name
      t.integer :car_2_id
      t.string :car_2_name
      t.integer :car_3_id
      t.string :car_3_name
      t.integer :car_4_id
      t.string :car_4_name
      t.integer :car_5_id
      t.string :car_5_name
      t.integer :car_6_id
      t.string :car_6_name
      t.integer :car_7_id
      t.string :car_7_name
      t.integer :car_8_id
      t.string :car_8_name
      t.integer :car_9_id
      t.string :car_9_name
      t.integer :car_10_id
      t.string :car_10_name
      t.string :date

      t.timestamps
    end
  end
end
