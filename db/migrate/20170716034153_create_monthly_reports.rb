class CreateMonthlyReports < ActiveRecord::Migration[5.0]
  def change
    create_table :monthly_reports do |t|
      t.integer :car_id, null: false
      t.integer :price, comment: 'trieu dong'
      t.integer :sold_number, comment: 'chiec'
      t.string :time, comment: 'yyyymm'

      t.integer :lock_version
      t.timestamps
    end
  end
end
