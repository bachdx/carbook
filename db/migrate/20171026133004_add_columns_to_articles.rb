class AddColumnsToArticles < ActiveRecord::Migration[5.1]
  def change
    add_column :articles, :image_width, :integer, after: :image_url
    add_column :articles, :image_height, :integer, after: :image_width
  end
end
