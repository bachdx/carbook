class CreateVideos < ActiveRecord::Migration[5.0]
  def change
    create_table :videos do |t|
      t.integer :car_id, null: false
      t.string :url

      t.integer :lock_version
      t.timestamps
    end
  end
end
