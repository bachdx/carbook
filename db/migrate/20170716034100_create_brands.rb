class CreateBrands < ActiveRecord::Migration[5.0]
  def change
    create_table :brands do |t|
      t.string :name
      t.text :desc

      t.integer :lock_version
      t.timestamps
    end
  end
end
